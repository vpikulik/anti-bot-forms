#!/usr/bin/env python

from distutils.core import setup

setup(
    name='AntiBotForms',
    version='1.1',
    description='Anti bot forms',
    author='Vitaly Pikulik',
    author_email='v.pikulik@gmail.com',
    url='https://bitbucket.org/vpikulik/anti-bot-forms',
    packages=('ABForms',),
    install_requires=['mock', 'six'],
    include_package_data = True,
    package_data = {
        '': ['static/*.js'],
    },
)
