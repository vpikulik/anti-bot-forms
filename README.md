Anti bot forms
==============

This form protects you from bots.

Install
-------

Install with pip
```bash
pip install git+https://bitbucket.org/vpikulik/anti-bot-forms.git
``` 

Add to django settings
```python
INSTALLED_APPS = (
    ...
    'ABForms',
    ...
)
```

How to use
----------

Create your form
```python
from ABForms.forms import ABForm, ABModelForm

class Form1(ABForm):
    .....

class ModelForm1(ABModelForm):
    .....
    class Meta:
        model = Model1
        .....
```

In templates:
```html
    <form .....>
        {{ form.media }}
        {% for hidden in form.hidden_fields %}
            {{ hidden }}
        {% endfor %}
        
        .....
    </form>
```

Tests
-----

```bash
python -m unittest discover
```
