
import hashlib
import six
from django import forms
from django.db.models import Model


_secret_field = forms.CharField(min_length=32, max_length=32, required=True,
                                widget=forms.HiddenInput)


def _convert_type(value):
    if isinstance(value, Model):
        return str(value.id)
    elif isinstance(value, six.text_type) and six.PY2:
        return str(value.encode('utf-8'))
    return str(value)


def clean_secret(cleaned_data):
    items = [
        (key, _convert_type(value))
        for key, value in cleaned_data.items()
        if key != 'secret'
    ]  # filter secret
    items = sorted(items, key=lambda x: x[0])  # sort by keys
    items = [value[1] for value in items if value[1]]
    src = ' '.join(items)
    if six.PY3:
        src = src.encode()
    secret = hashlib.md5(src).hexdigest()
    if secret != cleaned_data.get('secret'):
        raise forms.ValidationError('Wrong secret')

    return cleaned_data


class ABForm(forms.Form):

    secret = _secret_field

    def clean(self):
        cleaned_data = super(ABForm, self).clean()
        return clean_secret(cleaned_data)

    class Media:
        js = ('abforms.js', 'jsmd5.js')


class ABModelForm(forms.ModelForm):

    secret = _secret_field

    def clean(self):
        cleaned_data = super(ABModelForm, self).clean()
        return clean_secret(cleaned_data)

    class Media:
        js = ('abforms.js', 'jsmd5.js')
