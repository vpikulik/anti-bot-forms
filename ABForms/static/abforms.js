jQuery(document).ready(function( $ ) {
    $('input[name="secret"]').parents('form').submit(function() {
        var form = $(this);
        var fields = form.find('input,textarea,select');
        var data = {};
        var keys = [];
        for (var i=0; i<fields.length; i++) {
            var field = fields.eq(i);
            var value = field.val();
            if (field.is('input[type="checkbox"]')) {
                value = field.is(':checked') ? 'True' : 'False';
            }
            var name = field.attr('name');
            if (name && name != 'secret') {
                keys.push(name);
                data[name] = value;
            }
        }

        keys.sort();
        var values = [];
        for (var i in keys) {
            var key = keys[i];
            var value = data[key];
            if (value != '') {
                values.push(value);
            } 
        }

        var hash = MD5(values.join(' '));
        form.find('input[name="secret"]').val(hash);
        // console.log(hash, values.join(' '));
    });
});