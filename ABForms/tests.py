# -*- coding: utf-8 -*-
import hashlib
import six

from django.conf import settings
settings.configure(
    DATABASES={'default': {}},
    USE_I18N=False
)  # noqa

from django import forms
from mock import patch
from unittest import TestCase

from ABForms.forms import ABForm, clean_secret


def _get_data(**delta):
    data = {
        'text_field': u'Test text текст',
        'empty_field': '',
        'int_field': 22,
        'bool_field': True,
    }
    data.update(delta)
    return data


class TestClean(TestCase):

    def test_no_secret(self):
        self.assertRaises(
            forms.ValidationError,
            clean_secret,
            _get_data()
        )

    def test_empty_secret(self):
        self.assertRaises(
            forms.ValidationError,
            clean_secret,
            _get_data(secret='')
        )

    def test_wrong_secret(self):
        self.assertRaises(
            forms.ValidationError,
            clean_secret,
            _get_data(secret='wrong')
        )

    def test_right_data(self):
        src = 'True 22 Test text текст'
        if six.PY3:
            src = src.encode()
        secret = hashlib.md5(src).hexdigest()
        cleaned_data = _get_data(secret=secret)
        self.assertEquals(
            clean_secret(cleaned_data),
            cleaned_data
        )

    def test_right_data_with_zero(self):
        src = 'True 0 Test text текст'
        if six.PY3:
            src = src.encode()
        secret = hashlib.md5(src).hexdigest()
        cleaned_data = _get_data(secret=secret, int_field=0)
        self.assertEquals(
            clean_secret(cleaned_data),
            cleaned_data
        )


class TestABForm(TestCase):

    class TstForm(ABForm):

        text_field = forms.CharField(max_length=255)
        empty_field = forms.CharField(max_length=255, required=False)
        int_field = forms.IntegerField()
        bool_field = forms.BooleanField()

    @patch('ABForms.forms.clean_secret')
    def test_right_data(self, clean_mock):
        src = 'True 22 Test text текст'
        if six.PY3:
            src = src.encode()
        secret = hashlib.md5(src).hexdigest()
        cleaned_data = _get_data(secret=secret)
        form = self.TstForm(cleaned_data)
        self.assertTrue(form.is_valid())
        clean_mock.assert_called_once_with(cleaned_data)
